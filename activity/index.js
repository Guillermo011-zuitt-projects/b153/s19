let number = 5;

let getCube = Math.pow(number, 3)
console.log(`The cube of ${number} is ${getCube}`);

let address = [ "258 Washington Ave NW, North Carolina 90011"]

let [place] = address

console.log(`I live at ${place}`);

let animal = {
	name: "Blue Whale",
	species: "Mammal",
	habitat: "Water",
	food: "Seals",
}

let {name, species, habitat, food }= animal
console.log(`A ${name} is a ${species}. It lives in ${habitat}, and it's favorite food is ${food}`)

let numbers = [1, 2, 3, 4, 5]

const loop = () => numbers.forEach(function(count){
	console.log(numbers[count])
})
loop();

let reduceNumber = [1, 2, 3, 4, 5]
const reducer = (first, second) => first + second;
console.log(reduceNumber.reduce(reducer))