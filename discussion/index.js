// ES6 Updates

// We are using here Match Objects 
// - documentation - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math

// Exponent Operator
let firstNum = 8 ** 2
console.log(firstNum);

// Second way to write exponent operator
let secondNum = Math.pow(8, 2)
console.log(secondNum);

/* Template Literals*/
// Concatenation
let greeting = "Hello! ";
console.log(greeting + "My name is" + " Guillermo")

let song = "Careless Whisper";
let artist = "George Michael";
console.log("My favorite song is " + song + " by " + artist)
// using Template literals (using back ticks (``))
console.log(`My favorite song is ${song} by ${artist}`)

let myObj = {
	name: "Jino"
}
// object destructuring, it isolates the property from the object
// purpose of destructuring - 
let { name } = myObj;

name = "Joe";
console.log(name);
console.log(myObj.name);

const person = {
	firstName: "John",
	middleName: "Jacob",
	lastName: "Smith"
}

let = {firstName, middleName, lastName } = person
console.log(firstName);
console.log(middleName);
console.log(lastName);

// Array Destructuring

const people = ["John", "Joe", "Jack"]

let [firstPerson, secondPerson, thirdPerson] = people

console.log(`The first person is ${firstPerson}`)
console.log(`The second person is ${secondPerson}`)
console.log(`The third person is ${thirdPerson}`)

// Using the Object.keys() method to loop through objects
// we use th person object at the top for this
// Object.keys() method will create an array of the properties of an object

const personKeys = Object.keys(person)
console.log(personKeys);

personKeys.forEach(function(key){
	console.log(typeof person[key])
})

// way to call properties 

// Object.values() method will create an array of the values of an object passed as an argument

/*
	Arrow Functions - compact alternative syntax to writing traditional functions
	const functionName = () => { code to execute }
					- have what are called "implicit return statements" - but this is not always true, so make sure still to add return

*/

const sayHello = () => {
	console.log("Hello")
}
const addNum = num => num + num
// The parenthesis around the argument can be removed if exactly ONE argument is needed for the function
// Curly Braces can be removed if function only needs to execute a single statement

const add = (x, y) => x + y
let total = add(1, 2)
console.log(total)